# HKCycleViewSwift

[![CI Status](https://img.shields.io/travis/HJT916109796/HKCycleViewSwift.svg?style=flat)](https://travis-ci.org/HJT916109796/HKCycleViewSwift)
[![Version](https://img.shields.io/cocoapods/v/HKCycleViewSwift.svg?style=flat)](https://cocoapods.org/pods/HKCycleViewSwift)
[![License](https://img.shields.io/cocoapods/l/HKCycleViewSwift.svg?style=flat)](https://cocoapods.org/pods/HKCycleViewSwift)
[![Platform](https://img.shields.io/cocoapods/p/HKCycleViewSwift.svg?style=flat)](https://cocoapods.org/pods/HKCycleViewSwift)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

```swift
import UIKit
import HKCycleViewSwift
class BCBannerCell: UICollectionViewCell,UICollectionViewDelegate {
    lazy var cycleView: HKCycleView = {
        let view:HKCycleView = HKCycleView()
        let images:[UIImage] = [R.image.pic1() ?? UIImage(),R.image.pic2()  ?? UIImage(),R.image.pic3() ?? UIImage(),R.image.pic4() ?? UIImage()]
        view.setUrlsGroup(["http://chatm-icon.oss-cn-beijing.aliyuncs.com/pic/pic_20171101181927887.jpg", "http://chatm-icon.oss-cn-beijing.aliyuncs.com/pic/pic_20171114171645011.jpg", "http://chatm-icon.oss-cn-beijing.aliyuncs.com/pic/pic_20171114172009707.png"])
        view.setImagesGroup(images, titlesGroup: ["天天特价 -- 超值量贩，底价疯抢天天特价","一缕情丝，一缕温暖","快速匹配，及时推送","气质春装，一件包邮"])
        view.itemSize = CGSize(width: BC_SCREEN_WIDTH-80, height: (BC_SCREEN_WIDTH-80)*360/750)
        view.itemSpacing = 30
        view.itemZoomScale = 1.1
        view.itemCornerRadius = 10
        view.pageControlItemSize = CGSize.zero
        view.backgroundColor = UIColor.white
        view.delegate = self
        return view
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addSubview(cycleView)
        cycleView.frame = CGRect(x: 0, y: 0, width: BCBannerCell.itemHeight().width, height: BCBannerCell.itemHeight().height)
    }
    
    public static func itemHeight() -> CGSize {
        return CGSize(width: BC_SCREEN_WIDTH, height: 360.scale)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
//代理方法
extension UICollectionViewCell: HKCycleViewProtocol {
    public func hk_cycleViewDidScrollToIndex(_ index: Int) {
        
    }
    public func hk_cycleViewDidSelectedIndex(_ index: Int) {
        BCKeyWindow?.bc_showToast("你点击了第 \(index)个")
    }
}
```

## 效果图
![轮播-有文字-Swift](https://images.gitee.com/uploads/images/2019/0618/195716_e155425e_1908551.gif "轮播-Swift.gif")
![轮播-没有文字-Swift](https://images.gitee.com/uploads/images/2019/0618/195804_7e1e3f5f_1908551.gif "轮播-没有文字-Swift.gif")


## Requirements

## Installation

HKCycleViewSwift is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
#轮播组件
pod 'HKCycleViewSwift', :git => 'https://gitee.com/Steven_Hu/HKCycleViewSwift.git'
```

## Author

HJT916109796, hujintao@hezi.com

## License

HKCycleViewSwift is available under the MIT license. See the LICENSE file for more info.
